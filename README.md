# OpenML dataset: avocado-sales

https://www.openml.org/d/41210

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Historical data on avocado prices and sales volume in multiple US markets. Downloaded from Kaggle [https://www.kaggle.com/neuromusic/avocado-prices/home] on 29.10.2018. The original data stems from the HASS AVOCADO BOARD [http://www.hassavocadoboard.com/retail/volume-and-price-data]. The Kaggle dataset was licensed under the Open Database License (ODbL) [https://opendatacommons.org/licenses/odbl/1.0/]. The variable 'AveragePrice' was selected as target variable. For a description of all variables checkout the Kaggle dataset repo or the original dataset description by the HASS AVOCADO BOARD. 'Year' is coded as a categorical features as the dataset covers only the years 2015-2018. The dataset also includes a 'Date' variable (ignored by default) which can be used to construct additional month or day features. The ID variable from the Kaggle version was removed from the dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41210) of an [OpenML dataset](https://www.openml.org/d/41210). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41210/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41210/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41210/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

